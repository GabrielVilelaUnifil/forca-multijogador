import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class main {
    public static void main(String[] args) throws IOException{
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Digite o seu nick: ");
            String nick = sc.nextLine();

            InetAddress ip = InetAddress.getByName("localhost");

            Socket s = new Socket(ip, 16000);
            DataInputStream dis = new DataInputStream(s.getInputStream());
            DataOutputStream dos = new DataOutputStream(s.getOutputStream());

            dos.writeUTF(nick);
            System.out.println(dis.readUTF());

            Thread listener = new Thread("listener") {
                public void run(){
                    while (true) {
                        try {
                            System.out.print(dis.readUTF());
                        }catch (Exception e){
                            e.getMessage();
                        }
                    }
                }
            };

            Thread sender = new Thread("sender") {
                public void run(){
                    while (true) {
                        try{
                        String input = sc.nextLine();
                        dos.writeUTF(input);

                        if(input.equals(":sair"))
                        {
                            System.out.println("Fechando conexão com cliente " + s);
                            s.close();
                            System.out.println("Conexão encerrada.");
                            break;
                        }
                        }catch (Exception e){
                            e.getMessage();
                        }
                    }
                }
            };

            sender.start();
            listener.start();



        } catch (Exception e){
            e.getMessage();
        }
    }
}
