import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.Random;

public class main {
    public static void main(String[] args) throws IOException {
        ServerSocket endereco = new ServerSocket(16000);
        InputStream dicionario = main.class.getResourceAsStream("palavras/dicionario.txt");
        server sv = new server(dicionario);
        ArrayList<clientHandler> clients = new ArrayList<>();


                while (true) {
                    try {
                        Socket c = endereco.accept();
                        System.out.println("Um novo cliente foi conectado! Cliente " + c);

                        DataInputStream dis = new DataInputStream(c.getInputStream());
                        DataOutputStream dos = new DataOutputStream(c.getOutputStream());

                        System.out.println("Dando um Thread para cliente");
                        clientHandler cH = new clientHandler(c, dis, dos, sv);
                        clients.add(cH);
                        Thread t = cH;


                        t.start();

                    } catch (Exception e){
                        e.getMessage();
                    }
                }
            }

}