public class connectedClient {

    String nome;
    int pontos = 0;
    boolean online = false;

    public connectedClient(String nome){
        this.nome = nome;
        online = true;
    }

    public connectedClient(String nome, int pontos){
        this.nome = nome;
        this.pontos = pontos;
        online = true;
    }

    public String getNome(){
        return nome;
    }

    public int getPontos(){
        return pontos;
    }

    public void setPontos(int pontos){
        this.pontos = pontos;
    }

    public void addPontos(int add){
        this.pontos += add;
    }

    public void removePontos(int rem){
        this.pontos -= rem;
    }

    public void setOffline(){online = false;}

    public void setOnline(){online = true;}

    public boolean isOnline(){return online;}

}
