import java.util.Comparator;

public class sortByPontos implements Comparator<connectedClient> {
    public int compare(connectedClient a, connectedClient b){
        if(a.pontos > b.pontos){
            return -1;
        } else {
            return 1;
        }
    }
}
