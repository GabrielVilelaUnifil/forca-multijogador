import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class server {

    ArrayList<connectedClient> jogadores = new ArrayList<>();
    ArrayList<String> palavras = new ArrayList<>();
    ArrayList<rodada> rodadas = new ArrayList<>();
    int erros;
    String[] globalMessage = {"", "0"};

    public server(InputStream dicionario) {
        try {
            processarDicionario(dicionario);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        criarNovaRodada();
    }

    private void processarDicionario(InputStream d) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(d));
        String st;
        while((st = br.readLine()) != null){
            palavras.add(st);
        }
    }

    public void adicionarJogador(connectedClient jogador){
        jogadores.add(jogador);
    }

    public boolean verificarExistenciaJogador(connectedClient jogador){
        boolean jaExiste = false;
        for(int i = 0; i < jogadores.size(); i++){
            if(jogador.nome.equals(jogadores.get(i).nome)){
                jaExiste = true;
                jogadores.get(i).setOnline();
            }
        }
        return jaExiste;
    }

    public connectedClient getConnectedClient(String nome){
        for(int i = 0; i < jogadores.size(); i++){
            if(nome.equals(jogadores.get(i).nome)){
                jogadores.get(i).setOnline();
                return jogadores.get(i);
            }
        }
        return null;
    }

    public int verificarPontuacao(connectedClient jogador){
        for(int i = 0; i < jogadores.size(); i++){
            if(jogador.nome == jogadores.get(i).nome)
                return jogadores.get(i).getPontos();
        }
        return 0;
    }

    private void criarNovaRodada(){
        rodada r = new rodada(palavras.get((int) Math.floor(Math.random() * palavras.size())));
        rodadas.add(r);
    }

    public synchronized String[] getGlobalMessage(){
            return globalMessage;
    }

    public synchronized String adivinhar(char letra, connectedClient jogador){
        rodada currRodada = rodadas.get(rodadas.size()-1);
        int pointChanges = currRodada.addLetraAdv(letra);
        jogador.setPontos(jogador.getPontos() + pointChanges);

        String retorno = "";
        String[] globalMessageOld = globalMessage;
        String[] globalMessageNew = new String[2];
        globalMessageNew[0] = jogador.nome + " chutou e teve uma mudança de " + pointChanges + " pontos!\n";
        globalMessageNew[1] = globalMessageOld[1];





        if (pointChanges < 0){
            erros += 1;
        }

        retorno += jogador.nome + " chutou e teve uma mudança de " + pointChanges + " pontos!\n";

        if(erros >= 7){
            retorno += "Juntos os jogadores erraram 7 vezes, todos perdem 5 pontos!\n";
            globalMessageNew[0] += "Juntos os jogadores erraram 7 vezes, todos perdem 5 pontos!\n";
            erros = 0;
            for(int i = 0; i < jogadores.size(); i++){
                jogadores.get(i).removePontos(5);
            }
        }

        if(currRodada.checkForRodadaEnd()){
            retorno += "A palavra foi adivinhada! A palavra era: " + currRodada.palavra + "\nTodos os jogadores ganham cinco pontos! Iniciando nova rodada...\n";
            globalMessageNew[0] += "A palavra foi adivinhada! A palavra era: " + currRodada.palavra + "\nTodos os jogadores ganham cinco pontos! Iniciando nova rodada...\n";
            for(int i = 0; i < jogadores.size(); i++){
                jogadores.get(i).addPontos(5);
            }
            criarNovaRodada();
        }
        if (globalMessageOld == globalMessageNew){
            globalMessage[1] = String.valueOf(Integer.valueOf(globalMessage[1]) + 1);
        } else {
            globalMessageNew[1] = String.valueOf(0);
            globalMessage = globalMessageNew;
        }

        return retorno;

    }

    public String getRanking(){
        String result = "";
        ArrayList<connectedClient> cSorted = new ArrayList<>(jogadores);
        Collections.sort(cSorted, new sortByPontos());
        int offset = 0;

        for(int i = 0; i < cSorted.size(); i++){
            if(cSorted.get(i).isOnline()){
                connectedClient currconnectedClient = cSorted.get(i);
                result += ((i+1+offset)+"º Lugar: "+currconnectedClient.getNome()+ " com "+currconnectedClient.getPontos()+" pontos.\n");
            } else {
                offset -= 1;
            }
        }

        return result;
    }

    public String getDisplayCurrRodada(){
        return rodadas.get(rodadas.size()-1).getDisplay();
    }
}
