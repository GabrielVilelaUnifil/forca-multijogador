import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class clientHandler extends Thread{

    final DataInputStream dis;
    final DataOutputStream dos;
    final Socket c;
    final server sv;
    connectedClient cc;
    String[] lastMsg = {"",""};
    int runCount = 0;


    // Constructor
    public clientHandler(Socket c, DataInputStream dis, DataOutputStream dos, server sv)
    {
        this.c = c;
        this.dis = dis;
        this.dos = dos;
        this.sv = sv;
        lastMsg = sv.getGlobalMessage();
        try {
            String nome = dis.readUTF();
            if(sv.verificarExistenciaJogador(new connectedClient(nome))){
                this.cc = sv.getConnectedClient(nome);
            } else {
                this.cc = new connectedClient(nome);
                sv.adicionarJogador(cc);
            }

        } catch (Exception e){
            e.getMessage();
        }
    }

    @Override
    public void run()
    {
        try {
            dos.writeUTF("Digite a letra que deseja adivinhar: \n" + sv.getDisplayCurrRodada());
            runCount = 1;
        }catch (Exception e){
            e.getMessage();
        }

        Thread globalMessageSender = new Thread("messageSender") {
            public void run(){
                while (true) {

                        try {
                            if(runCount > 0) {
                                String[] glblMsg = sv.getGlobalMessage();
                                if ((glblMsg[0] == lastMsg[0] && glblMsg[1] == lastMsg[1]) || glblMsg[0].substring(0, glblMsg[0].indexOf(" ")).equals(cc.nome)) {
                                    lastMsg = glblMsg;
                                } else if (glblMsg[0] == lastMsg[0] && glblMsg[1] != lastMsg[1]) {
                                    dos.writeUTF(glblMsg[0] + "\nDigite a letra que deseja adivinhar: \n" + sv.getDisplayCurrRodada() + "\n");
                                    lastMsg = glblMsg;

                                } else {
                                    dos.writeUTF(glblMsg[0] + "\nDigite a letra que deseja adivinhar: \n" + sv.getDisplayCurrRodada() + "\n");
                                    lastMsg = glblMsg;
                                }
                            }
                            Thread.sleep(30);
                        } catch (Exception e) {
                            e.getMessage();
                        }
                    }

            }
        };

                globalMessageSender.start();
                while (true)
                {
                    try {
                        String received = "";
                        String toreturn = "";
                        // Ask user what he wants


                        // receive the answer from client
                        received = dis.readUTF();

                        if(received.equals(":sair"))
                        {
                            System.out.println("Cliente " + c + " enviou mensagem de saída");
                            System.out.println("Encerrando conexão ...");
                            cc.setOffline();
                            c.close();
                            System.out.println("Conexão encerrada");
                            break;
                        }



                        // write on output stream based on the
                        // answer from the client
                        switch (received) {
                            case ":pontos" :
                                toreturn += "Voce tem " + sv.verificarPontuacao(cc) + " pontos. \n";
                                break;

                            case ":ranking" :
                                toreturn += sv.getRanking() + "\n";

                                break;
                            case "" :
                                toreturn += "Voce precisa digitar alguma coisa!\n";
                                break;

                            default:
                                toreturn += sv.adivinhar(received.charAt(0), cc) + "\n";
                                break;
                        }
                        toreturn += "Digite a letra que deseja adivinhar: \n";
                        toreturn += sv.getDisplayCurrRodada()+ "\n";
                        dos.writeUTF(toreturn);
                    } catch (IOException e) {
                        break;
                    }


        }


        try
        {
            // closing resources
            this.dis.close();
            this.dos.close();

        }catch(IOException e){
            e.printStackTrace();
        }
    }

}
