import java.util.ArrayList;

public class rodada {
    char[] palavraAdv;
    String palavra;
    ArrayList<Character> letrasAdv = new ArrayList<>();
    ArrayList<Character> letrasAdvCert = new ArrayList<>();
    int erros = 0;
    int indexSpc = 9999;

    StringBuilder display = new StringBuilder("");

    public rodada(String palavra){
        palavraAdv = palavra.toCharArray();
        this.palavra = palavra;
        for (int i = 0; i < palavraAdv.length; i++){
            if(palavraAdv[i] == ' ') display.append(" ");
            else display.append("_");
        }
        if(palavra.indexOf(" ") >= 0){
            indexSpc = palavra.indexOf(" ");
        }
        String palavraNoSpc = palavra.replace(" ", "");
        palavraAdv = palavraNoSpc.toCharArray();
    }

    public int addLetraAdv(char letra){
        letra = Character.toLowerCase(letra);
        int changeInPontos = 0;
        boolean acertou = false;
        boolean jaEscolhido = false;

        for(int i = 0; i < letrasAdv.size(); i++){
            if(letra == letrasAdv.get(i)) jaEscolhido = true;
        }
        if(!jaEscolhido) {
            for (int i = 0; i < palavraAdv.length; i++) {
                if (letra == palavraAdv[i]) {
                    if(i >= indexSpc){
                        display.setCharAt((i+1), letra);
                    } else display.setCharAt(i, letra);
                    letrasAdvCert.add(letra);
                    changeInPontos += 1;
                    acertou = true;
                }

            }
        }


        letrasAdv.add(letra);
        if(jaEscolhido){
            changeInPontos = -1;
        } else if(!acertou){
            changeInPontos -= 3;
        }

        return changeInPontos;
    }

    public boolean checkForRodadaEnd(){

        if(letrasAdvCert.size() == palavraAdv.length){
            return true;
        } else return false;
    }


    public String getPalavra(){
        return palavra;
    }

    public String getDisplay(){
        return display.toString();
    }

}
