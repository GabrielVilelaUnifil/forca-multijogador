import java.lang.reflect.Array;
import java.util.ArrayList;
import java.io.*;
import java.util.Collections;
import java.util.Comparator;

public class server {

    ArrayList<client> jogadores = new ArrayList<>();
    ArrayList<String> palavras = new ArrayList<>();
    ArrayList<rodada> rodadas = new ArrayList<>();
    int erros;

    public server(File dicionario) {
        try {
            processarDicionario(dicionario);
        } catch (Exception e){
            System.out.println(e.getMessage());
        }

        criarNovaRodada();
    }

    private void processarDicionario(File d) throws Exception{
        BufferedReader br = new BufferedReader(new FileReader(d));
        String st;
        while((st = br.readLine()) != null){
            palavras.add(st);
        }
    }

    public void adicionarJogador(client jogador){
        boolean jaExiste = false;
        for(int i = 0; i < jogadores.size(); i++){
            if(jogador.nome == jogadores.get(i).nome){
                jaExiste = true;
            }
        }
        if(!jaExiste)
        jogadores.add(jogador);
    }

    public int verificarPontuacao(client jogador){
        for(int i = 0; i < jogadores.size(); i++){
            if(jogador.nome == jogadores.get(i).nome)
                return jogadores.get(i).getPontos();
        }
        return 0;
    }

    private void criarNovaRodada(){
        rodada r = new rodada(palavras.get((int) Math.floor(Math.random() * palavras.size())));
        rodadas.add(r);
    }

    public void adivinhar(char letra, client jogador){
        rodada currRodada = rodadas.get(rodadas.size()-1);
        int pointChanges = currRodada.addLetraAdv(letra);
        jogador.setPontos(jogador.getPontos() + pointChanges);

        if (pointChanges < 0){
            erros += 1;
        }

        if(erros >= 7){
            System.out.println("Juntos os jogadores erraram 7 vezes, todos perdem 5 pontos!");
            erros = 0;
            for(int i = 0; i < jogadores.size(); i++){
                jogadores.get(i).removePontos(5);
            }
        }

        if(currRodada.checkForRodadaEnd()){
            System.out.println("A palavra foi adivinhada! A palavra era: " + currRodada.palavra);
            System.out.println("Todos os jogadores ganham cinco pontos! Iniciando nova rodada...");
            for(int i = 0; i < jogadores.size(); i++){
                jogadores.get(i).addPontos(5);
            }
            criarNovaRodada();
        }

    }

    public String getRanking(){
        String result = "";
        ArrayList<client> cSorted = new ArrayList<>(jogadores);
        Collections.sort(cSorted, new sortByPontos());

        for(int i = 0; i < cSorted.size(); i++){
            client currClient = cSorted.get(i);
            result += ((i+1)+"º Lugar: "+currClient.getNome()+ " com "+currClient.getPontos()+" pontos.\n");
        }

        return result;
    }

    public String getDisplayCurrRodada(){
        return rodadas.get(rodadas.size()-1).getDisplay();
    }
}
