public class client {

    String nome;
    int pontos = 0;

    public client (String nome){
        this.nome = nome;
    }

    public client (String nome, int pontos){
        this.nome = nome;
        this.pontos = pontos;
    }

    public String getNome(){
        return nome;
    }

    public int getPontos(){
        return pontos;
    }

    public void setPontos(int pontos){
        this.pontos = pontos;
    }

    public void addPontos(int add){
        this.pontos += add;
    }

    public void removePontos(int rem){
        this.pontos -= rem;
    }

}
