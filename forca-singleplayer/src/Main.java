import java.util.Scanner;
import java.io.*;


public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite o seu nick: ");
        client c = new client(sc.nextLine());
        File dicionario = new File("palavras\\dicionario.txt");
        server sv = new server(dicionario);
        sv.adicionarJogador(c);

        System.out.println(sv.rodadas.get(sv.rodadas.size()-1).getDisplay());
        String st;
        System.out.print("Digite a letra que deseja adivinhar: ");
        while (!(st = sc.nextLine()).equals(":sair")){
            if(st.equals(":pontos")){
                System.out.println(sv.verificarPontuacao(c));
            } else if(st.equals(":ranking")){
                sv.adicionarJogador(new client("d", 7));
                sv.adicionarJogador(new client("a", 2));
                sv.adicionarJogador(new client("b", 4));
                sv.adicionarJogador(new client("e", 9));
                sv.adicionarJogador(new client("c", 5));

                System.out.println(sv.getRanking());
            } else {

                sv.adivinhar(st.charAt(0), c);
                //clearScreen();
            }
            System.out.println(sv.getDisplayCurrRodada());
            System.out.print("Digite a letra que deseja adivinhar: ");
        }
    }

    public static void clearScreen(){
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

}
