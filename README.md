# Forca Multijogador

## Requisitos
Java 11> 

## Compilação

Para compilar o projeto, siga os seguintes passos abaixo:

1. Faça o download do IDE IntelliJ IDEA 2020.2
2. Execute o IDE
3. Descompacte este projeto no diretório desejado
4. Navegue até a barra de ferramentas, localize o menu `Arquivo` e selecione a opção `Abrir`
5. Navegue até a pasta onde foi descompactado o projeto, selecione a pasta do projeto, selecione o a pasta forca-multijogador-server e clique em `OK`.
6. (Opt) Navegue até a pasta `src/palavras` e cole a entrada dentro do arquivo `dicionario.txt`
7. Voltando ao IntelliJ, no canto superior, clique no menu `Build`, e clique na opção `Build Project`
8. Feito isso, clique no menu `Build` novamente, selecione a opção `Build Artifacts`, passe o mouse sobre forca-multijogador-server:jar e finalmente clique na opção `Build`. Após isso, o arquivo executável .jar será gerado e ficará localizado na pasta `out/artifacts/forca_multijogador_server_jar`
9. Repita os passos 4 a 8, porém desta vez selecionando a pasta `forca-multijogador-client` no passo 4, e navegando até `out/artifacts/forca_multijogador_client_jar`

## Execução

Com ambos os .jar do client e do server em mãos, em um terminal de sua preferencia, navegue até a pasta do .jar do server, e faça o comando `java -jar forca-multijogador-server.jar`. Similarmente, em uma outra janela de um terminal de sua preferencia, navegue até a pasta do .jar do client, e faça o comando `java -jar forca-multijogador-client.jar`. Poderá abrir vários clients mas apenas um server.

Após isso, na(s) janela(s) do client, digite o seu nick (nome) a aperte enter para conectar ao server.

## Comandos


- :ranking
Exibe o placar de todos os jogadores conectados.

- :pontos
Exibe a sua pontuação.

- :sair
Termina a conexão